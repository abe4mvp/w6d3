var _ = require("./underscore");

// var sum = function(){
//   var args = Array.prototype.slice.call(arguments);
//   return _.reduce(args, function(total,num) {
//     return total + num;
//   }, 0);
// }

// console.log(sum(1, 2, 3, 4, 5))

Function.prototype.myBind = function(obj) {
  var args = Array.prototype.slice.call(arguments);
  var args = args.slice(1);

  var self = this;
  return function(){
    self.apply(obj, args);
  }
}

function Animal(name) {
  this.name = name;
}

Animal.prototype.makeNoise = function(noise) {
  console.log(this.name + " says " + noise);
}

// var cat = new Animal("Kat");
// var dog = new Animal("Scooby Doo");
//
// cat.makeNoise("meow");
// cat.makeNoise.myBind(dog, "meow")();

var curriedSum = function(numArgs){
  var numbers = [];
  var _curriedSum = function(num){
    numbers.push(num);
    if (numbers.length === numArgs){
      return _.reduce(numbers, function(total,el) {
        return total + el;
      }, 0);
    } else {
      return _curriedSum;
    }
  }

  return _curriedSum;
}

// var sum = curriedSum(4);
// console.log(sum(5)(30)(20)(1)); // => 56


Function.prototype.curry = function(numArgs){
  var self = this;
  var args = []
  var _curry = function(arg){

    // var args = Array.prototype.slice.call(arguments);
//     var args = args.slice(1);

    args.push(arg);
    if (args.length === numArgs){
      // return _.reduce(args, function(total,el) {
//         return total + el;
      // }, 0);
      return self.apply(null, args);
    } else {
      return _curry;
    }
  }

  return _curry;
}

var sum = function() {
  var sum = 0;

  var args = Array.prototype.slice.call(arguments);

  args.forEach(function(num) { sum += num; });
  return sum;
};

console.log(sum(5,30,20,1)); // => 56

console.log(sum.curry(4)(5)(30)(20)(1)); // => 56







